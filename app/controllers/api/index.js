/**
 * @file contains entry point of controllers api module
 * @author Wangi Wahyuni
 */

const main = require("./main");
const v1 = require("./v1");

module.exports = {
  main,
  v1,
};
