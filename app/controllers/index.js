/**
 * @file contains entry point of controllers module
 * @author Wangi Wahyuni
 */

const api = require("./api");
const main = require("./main");

module.exports = {
  api,
  main,
};
